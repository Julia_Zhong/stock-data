## Building a Stock Data app with Spring Boot and React

### Steps to Setup the Spring Boot Back end app (stock-data)

1. **Clone the application**

   ```bash
   git clone https://gitlab.com/Julia_Zhong/stock-data.git
   ```

2.  **Start the Postgres database**
   
   Go to the `stock-data` root folder -

   ```bash
   cd stock-data
   ```
   Execute this command for Docker to pull the postgres-db image -

!Note that it is required Docker Compose to be installed - see guide [here](https://docs.docker.com/compose/install/)

   ```bash
   docker-compose up
   ```

3.  **Run the app**

Now run the application -

   ```bash
   ./gradlew run
   ```
The back end server should start on port `8080`.

### Steps to setup the React front end app (stock-data-webapp)

First go to the `stock-data-webapp` folder -

```bash
cd stock-data-webapp
```

Then you can use the following commands:

   to install the dependencies - 
```bash
npm install
```
   and start the application -

```bash
npm start
```

The front-end server will start on port `3000`.