import React from 'react';
import './Table.css';

class DailyViewTable extends React.Component {

    state = {
        loading: true,
        topGained: [],
        topDeclined: [],
        mostActive: [],
        biggestTurnover: [],
    };

    componentDidMount() {
        this.fetchStockData(this.props.date);
    }

    async fetchStockData(date) {
        if (date !== '') {
            const url = "http://localhost:8080/stock-data/" + date;
            const response = await fetch(url);
            const data = await response.json();
            this.setState({
                topGained: data.topGained,
                topDeclined: data.topDeclined,
                mostActive: data.mostActive,
                biggestTurnover: data.biggestTurnover,
                loading: false
            });
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.date !== prevProps.date) {
            this.fetchStockData(this.props.date)
        }
    }

    render() {
        var topGained = this.state.topGained.map(top => (
                <tr key={top.id}>
                    <td>{top.name}</td>
                    <td style={{color:"green", fontWeight:"bold"} }>{top.priceChange}</td>
                </tr>
            )
        );
        var topDeclined = this.state.topDeclined.map(top => (
                <tr key={top.id}>
                    <td>{top.name}</td>
                    <td style={{color:"red", fontWeight:"bold"}}>{top.priceChange}</td>
                </tr>
            )
        );
        var mostActive = this.state.mostActive.map(top => (
                <tr key={top.id}>
                    <td>{top.name}</td>
                    <td>{top.trades}</td>
                </tr>
            )
        );
        var biggestTurnover = this.state.biggestTurnover.map(top => (
                <tr key={top.id}>
                    <td>{top.name}</td>
                    <td>{top.turnover}</td>
                </tr>
            )
        );
        return (
            <div>
                {this.state.loading || !this.state.topGained ? (
                    <div> loading... </div>
                ) : (
                    <div>
                        <div className="gained-declined">
                            <div>TOP GAINERS | DECLINE...</div>
                            <table>
                                <thead>
                                <tr>
                                    <th>Security</th>
                                    <th>Price Change(%)</th>
                                </tr>
                                </thead>
                                <tbody>
                                {topGained}
                                {topDeclined}
                                </tbody>
                            </table>
                        </div>
                        <div className="most-active">
                            <div>MOST ACTIVE</div>
                            <table >
                                <thead>
                                <tr>
                                    <th>Security</th>
                                    <th>Trades</th>
                                </tr>
                                </thead>
                                <tbody>
                                {mostActive}
                                </tbody>
                            </table>
                        </div>
                        <div className="biggest-turnover">
                            <div>BIGGEST TURNOVER</div>
                            <table>
                                <thead>
                                <tr>
                                    <th>Security</th>
                                    <th>Turnover</th>
                                </tr>
                                </thead>
                                <tbody>
                                {biggestTurnover}
                                </tbody>
                            </table>
                        </div>
                    </div>
                )}
            </div>
        );
    }
}


export default DailyViewTable;