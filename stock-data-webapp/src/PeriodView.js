import React from 'react';
import Cell from "antd/es/descriptions/Cell";

class PeriodView extends React.Component {

    state = {
        loading: true,
        mostActive: [],
    };

    componentDidMount() {
        this.fetchStockData(this.props.period);
    }

    async fetchStockData(period) {
        if(period[0] !== '' && period[1] !== ''){
            if(new Date(period[0]) > new Date(period[1])){
                alert("Start date cannot be bigger than End date!")
            }
            console.log(period[0], period[1])
            const url = "http://localhost:8080/stock-data/period?dateFrom="+(period[0]) + "&dateTo=" + (period[1]);
            const response = await fetch(url);
            const data = await response.json();
            this.setState({
                mostActive: data.mostActiveList,
                loading: false
            });

        }
    }

    componentDidUpdate(prevProps) {
        if(this.props.period !==prevProps.period){
            this.fetchStockData(this.props.period);
        }
    }


    render() {
        var i = 1;
        var mostActive = this.state.mostActive.map(top => (
                <tr key={top.id}>
                    <td>{i++}</td>
                    <td>{top.name}</td>
                    <td>{top.volume}</td>
                </tr>
            )
        );
        return (
            <div className="Period">
                {this.state.loading || !this.state.mostActive ? (
                  ''
                ) : (
                    <div className="period-table">
                        <table>
                            <thead >
                            <tr >
                                <th></th>
                                <th>Security</th>
                                <th>Volume</th>
                            </tr>
                            </thead>
                            <tbody>
                            {mostActive}
                            </tbody>
                        </table>
                    </div>
                )}
            </div>
        );
    }
}


export default PeriodView;