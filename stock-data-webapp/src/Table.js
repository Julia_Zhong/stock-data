import React from 'react';
import StockDataTable from './StockDataTable'
import DailyViewTable from "./DailyViewTable";
import PeriodView from "./PeriodView";
import './Table.css';

class Table extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dailyView: false,
            periodView: false,
        }
    }
componentDidMount() {
        console.log(this.state)
}

    reset = () =>{
        this.setState({dailyView: false,
            periodView: false,});
    }

    componentDidUpdate(prevProps) {
        if (this.props.period !== prevProps.period) {
            this.setState({dailyView: false, periodView: true})
        }
        if (this.props.date !== prevProps.date) {
            this.setState({dailyView: true, periodView: false})
        }
    }

    render() {

        return (
            <div className="Table">
                <div className="Table-body">
                    <button onClick={this.reset}>All days chart</button>
                    {this.state.periodView ? <PeriodView period={this.props.period}/> : ''}
                    {this.state.dailyView ? <DailyViewTable date={this.props.date}/> : ''}
                    {!this.state.periodView && !this.state.dailyView ? <StockDataTable/> : ''}
                </div>
            </div>
        );
    }
}

export default Table;


