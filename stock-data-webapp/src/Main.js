import React, {Component} from 'react';
import './Main.css'
import Table from "./Table";
import {ReactComponent as Logo} from'./lhv-logo.svg'


class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date: '',
            availableDates: [],
            startDate: '',
            endDate: '',
        };
        this.handleSelect = this.handleSelect.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleSelect(date) {
        this.setState({date: date.target.value});
    }

    handleChange(event) {
        console.log("NAME: " + event.target.name + " VALUE: " + event.target.value)
        this.setState(
            {[event.target.name]: event.target.value}
        );
    }


    async componentDidMount() {
        const url = "http://localhost:8080/entry-date/";
        const response = await fetch(url);
        const data = await response.json();
        this.setState({availableDates: data.availableDates});
    }

    reset = () =>{
        this.setState({dailyView: false,
            periodView: false,});
    }

    render() {
        var dates = this.state.availableDates.map(date =>
            <option key={date.id}> {date.date}</option>
        );

        return (
            <div className="Main">
                <div>
                    <header className="header">
                        <div className="title">
                            <Logo/>

                        </div>
                    </header>
                    <div className="navbar">
                    </div>
                    <div className="DatePicker">
                        <div><h4>Day statistics</h4></div>
                        <select className="form-control" name="date"
                                onChange={this.handleSelect}>
                            <option defaultValue=''>Select date...</option>
                            {dates}
                        </select>
                    </div>
                    <div className="PeriodPicker">
                        <div style={{marginRight:"250px"}}><h4>Most active for the period</h4></div>
                        <select className="form-control" name="startDate"
                                onChange={this.handleChange}>
                            <option defaultValue=''>Select start date...</option>
                            {dates}
                        </select>
                        <select className="form-control" name="endDate"
                                onChange={this.handleChange}>
                            <option defaultValue=''>Select end date...</option>
                            {dates}
                        </select>
                    </div>
                    <div>
                        <Table date={this.state.date}
                               period={[this.state.startDate, this.state.endDate]}
                        />
                    </div>
                </div>
            </div>
        );
    }

}

export default Main;