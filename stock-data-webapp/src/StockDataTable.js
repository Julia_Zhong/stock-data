import React, {Component} from 'react';
import './Table.css';

import StockData from "./StockData";


class StockDataTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading:true,
            stocks: [],
        };
    }
 async componentDidMount() {
            const url = "http://localhost:8080/stock-data/";
            const response = await fetch(url);
            const data = await response.json();
            this.setState({stocks: data.stockData, loading:false});
        }

  render(){
  var stocks = this.state.stocks.map(stock =>(
      <StockData key={stock.id} stock={stock} />
    )
  );
    return(

        <div>
           <div className="full-table">
               {this.state.loading || !this.state.stocks? (
                   ''
               ):(<table>
                   <thead>
                   <tr>
                       <th>Date</th>
                       <th>Name</th>
                       <th>Price Change(%)</th>
                       <th>Open Price</th>
                       <th>High Price</th>
                       <th>Low Price</th>
                       <th>Last close Price</th>
                       <th>Last Price</th>
                       <th>Best bid</th>
                       <th>Best ask</th>
                       <th>Trades</th>
                       <th>Volume</th>
                       <th>Turnover</th>
                   </tr>
                   </thead>
                   <tbody>
                   {stocks}
                   </tbody>
               </table>)}

            </div>

        </div>
    );
  }
}

export default StockDataTable;