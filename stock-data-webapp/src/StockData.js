import React, {Component} from 'react';

class StockData extends React.Component {
    constructor(props) {
        super(props);
    }
    render(){
        return (
            <tr>
                <td style={{width:"100px"}}>{this.props.stock.date}</td>
                <td>{this.props.stock.name}</td>
                <td style={{color: this.props.stock.priceChange < 0? "red":"green"}}>{this.props.stock.priceChange}</td>
                <td>{this.props.stock.openPrice}</td>
                <td>{this.props.stock.highPrice}</td>
                <td>{this.props.stock.lowPrice}</td>
                <td>{this.props.stock.lastClose}</td>
                <td>{this.props.stock.lastPrice}</td>
                <td>{this.props.stock.bestBid}</td>
                <td>{this.props.stock.bestAsk}</td>
                <td>{this.props.stock.trades}</td>
                <td>{this.props.stock.volume}</td>
                <td>{this.props.stock.turnover}</td>
            </tr>
        )
    }

}

export default StockData