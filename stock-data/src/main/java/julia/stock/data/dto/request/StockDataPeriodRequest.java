package julia.stock.data.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StockDataPeriodRequest {
    private LocalDate dateFrom;
    private LocalDate dateTo;
}