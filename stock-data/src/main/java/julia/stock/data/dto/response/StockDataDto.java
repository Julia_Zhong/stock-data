package julia.stock.data.dto.response;

import julia.stock.data.entity.EntryDate;
import julia.stock.data.entity.StockInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StockDataDto {
    private Long id;
    private String ticker;
    private String name;
    private String isin;
    private String currency;
    private String marketPlace;
    private String listOrSegment;
    private BigDecimal averagePrice;
    private BigDecimal openPrice;
    private BigDecimal highPrice;
    private BigDecimal lowPrice;
    private BigDecimal lastClose;
    private BigDecimal lastPrice;
    private BigDecimal priceChange;
    private BigDecimal bestBid;
    private BigDecimal bestAsk;
    private BigDecimal trades;
    private BigDecimal volume;
    private BigDecimal turnover;
    private String industry;
    private String supersector;
    private LocalDate date;

    public static StockDataDto convertToDto(StockInfo info) {
        return StockDataDto.builder()
                .id(info.getId())
                .ticker(info.getTicker())
                .name(info.getName())
                .isin(info.getIsin())
                .currency(info.getCurrency())
                .marketPlace(info.getMarketPlace())
                .listOrSegment(info.getListOrSegment())
                .averagePrice(info.getAveragePrice())
                .openPrice(info.getOpenPrice())
                .highPrice(info.getHighPrice())
                .lowPrice(info.getLowPrice())
                .lastClose(info.getLastClose())
                .lastPrice(info.getLastPrice())
                .priceChange(info.getPriceChange())
                .bestBid(info.getBestBid())
                .bestAsk(info.getBestAsk())
                .trades(info.getTrades())
                .volume(info.getVolume())
                .turnover(info.getTurnover())
                .industry(info.getIndustry())
                .supersector(info.getSupersector())
                .date(info.getDateEntry().getDate())
        .build();
    }
}
