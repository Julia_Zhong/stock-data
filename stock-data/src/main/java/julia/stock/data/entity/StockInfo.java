package julia.stock.data.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Map;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "stock_data")
public class StockInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "ticker")
    private String ticker;
    @Column(name = "name")
    private String name;
    @Column(name = "isin")
    private String isin;
    @Column(name = "currency")
    private String currency;
    @Column(name = "market_place")
    private String marketPlace;
    @Column(name = "list_or_segment")
    private String listOrSegment;
    @Column(name = "average_price")
    private BigDecimal averagePrice;
    @Column(name = "open_price")
    private BigDecimal openPrice;
    @Column(name = "high_price")
    private BigDecimal highPrice;
    @Column(name = "low_price")
    private BigDecimal lowPrice;
    @Column(name = "last_close_price")
    private BigDecimal lastClose;
    @Column(name = "last_price")
    private BigDecimal lastPrice;
    @Column(name = "price_change")
    private BigDecimal priceChange;
    @Column(name = "best_bid")
    private BigDecimal bestBid;
    @Column(name = "best_ask")
    private BigDecimal bestAsk;
    @Column(name = "trades")
    private BigDecimal trades;
    @Column(name = "volume")
    private BigDecimal volume;
    @Column(name = "turnover")
    private BigDecimal turnover;
    @Column(name = "industry")
    private String industry;
    @Column(name = "supersector")
    private String supersector;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "date_id")
    private EntryDate dateEntry;
    @Column(name = "created")
    private Instant created;
    @Column(name = "updated")
    private Instant updated;


    public static StockInfo createEntity(Map<Integer, String> values, EntryDate dateEntry) {
        return builder()
                .ticker(values.get(0))
                .name(values.get(1))
                .isin(values.get(2))
                .currency(values.get(3))
                .marketPlace(values.get(4))
                .listOrSegment(values.get(5))
                .averagePrice(setBigDecimal(values.get(6)))
                .openPrice(setBigDecimal(values.get(7)))
                .highPrice(setBigDecimal(values.get(8)))
                .lowPrice(setBigDecimal(values.get(9)))
                .lastClose(setBigDecimal(values.get(10)))
                .lastPrice(setBigDecimal(values.get(11)))
                .priceChange(setBigDecimal(values.get(12)))
                .bestBid(setBigDecimal(values.get(13)))
                .bestAsk(setBigDecimal(values.get(14)))
                .trades(setBigDecimal(values.get(15)))
                .volume(setBigDecimal(values.get(16)))
                .turnover(setBigDecimal(values.get(17)))
                .industry(values.get(18))
                .supersector(values.get(19))
                .dateEntry(dateEntry)
                .build();
    }

    private static BigDecimal setBigDecimal(String cellValue) {
        if (!cellValue.isBlank()) {
            return new BigDecimal(cellValue);
        }
        return null;
    }
}
