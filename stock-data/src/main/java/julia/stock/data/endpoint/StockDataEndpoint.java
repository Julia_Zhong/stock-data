package julia.stock.data.endpoint;

import julia.stock.data.dto.response.MostActiveResponse;
import julia.stock.data.service.EntryDateService;
import julia.stock.data.service.StockInfoService;
import julia.stock.data.dto.response.EntryDateResponse;
import julia.stock.data.dto.response.StockDataDailyResponse;
import julia.stock.data.dto.response.StockDataResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class StockDataEndpoint {
    private final StockInfoService stockInfoService;
    private final EntryDateService entryDateService;

    @GetMapping("/stock-data")
    public ResponseEntity<StockDataResponse> getStockData() {
        return ResponseEntity.ok(stockInfoService.getStockData());
    }

    @GetMapping("/stock-data/{date}")
    public ResponseEntity<StockDataDailyResponse> getStockDataForDate(@PathVariable String date) {
        return ResponseEntity.ok(stockInfoService.getDailyData(date));
    }
    @GetMapping("/stock-data/current")
    public ResponseEntity<StockDataResponse> getCurrentStockData() {
        return ResponseEntity.ok(stockInfoService.getCurrentStockData());
    }

    @GetMapping("/entry-date")
    public ResponseEntity<EntryDateResponse> getAvailableDates() {
        return ResponseEntity.ok(entryDateService.getAvailableDates());
    }

    @GetMapping("/stock-data/period")
    public ResponseEntity<MostActiveResponse> getStockDataForPeriod(@RequestParam String dateFrom, @RequestParam String dateTo) {
        return ResponseEntity.ok(stockInfoService.getStockDataForPeriod(dateFrom, dateTo));
    }
}
