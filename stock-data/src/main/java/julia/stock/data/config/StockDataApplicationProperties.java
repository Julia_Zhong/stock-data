package julia.stock.data.config;

import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Component
@Validated
@ConfigurationProperties("stock.data")
public class StockDataApplicationProperties {

    private String endpoint;
}
