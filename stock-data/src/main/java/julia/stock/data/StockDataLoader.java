package julia.stock.data;

import julia.stock.data.config.StockDataApplicationProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.File;
import java.nio.file.Files;
import java.time.LocalDate;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Component
public class StockDataLoader {
    private final RestTemplate restTemplate;
    private final StockDataFileParser fileParser;
    private final StockDataApplicationProperties properties;


    public void downloadDataFile(LocalDate date) {
        log.info("Downloading file for date: " + date);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(List.of(MediaType.APPLICATION_OCTET_STREAM));

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(properties.getEndpoint())
                    .queryParam("download", 1)
                    .queryParam("date", date);

            HttpEntity<String> entity = new HttpEntity<>(headers);
            ResponseEntity<byte[]> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity, byte[].class);

            if (response.getBody() != null) {
                File tempFile = File.createTempFile(date.toString(), ".xlsx");
                fileParser.readFile(Files.write(tempFile.toPath(), response.getBody()), date);
                tempFile.deleteOnExit();
            }
        } catch (Exception e) {
            log.error("Error occurred while downloading file for date " + date, e );
        }
    }
}
