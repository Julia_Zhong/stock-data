package julia.stock.data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;

@EnableConfigurationProperties
@ComponentScan
@SpringBootApplication
public class StockDataApplication {
    public static void main(String[] args) {
        SpringApplication.run(StockDataApplication.class, args);
    }
}
