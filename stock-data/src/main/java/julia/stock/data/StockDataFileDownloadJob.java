package julia.stock.data;

import julia.stock.data.entity.EntryDate;
import julia.stock.data.repository.EntryDateRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
@RequiredArgsConstructor
@Component
public class StockDataFileDownloadJob {
    private final EntryDateRepository dateEntryRepository;
    private final StockDataLoader loader;

    @Scheduled(initialDelay = 10L * 1000L, fixedDelay = 1800L * 1000L)
    void downloadStockDataFiles() {
        log.info("Starting file downloading job.");

        List<LocalDate> dateList = dateEntryRepository.findAll().isEmpty() ?
                getInitialDateList() : getExistingDateList();

        if (dateList.size() <= 1)
            loader.downloadDataFile(LocalDate.now());
        else
            dateList.forEach(loader::downloadDataFile);

    }

    private List<LocalDate> getExistingDateList() {
        EntryDate startDate = dateEntryRepository.findTopByOrderByIdDesc();
        log.debug("StartDate: " + startDate + "Dates until: " + startDate.getDate().datesUntil(LocalDate.now()));
        return startDate.getDate().datesUntil(LocalDate.now())
                .filter(this::isWeekendDay)
                .collect(Collectors.toList());
    }


    private List<LocalDate> getInitialDateList() {
        LocalDate tenDaysBeforeToday = LocalDate.now().minusDays(10);

        return IntStream.rangeClosed(0, 10)
                .mapToObj(tenDaysBeforeToday::plusDays)
                .filter(this::isWeekendDay)
                .collect(Collectors.toList());
    }


    private boolean isWeekendDay(LocalDate date) {
        return date.getDayOfWeek() != DayOfWeek.SATURDAY && date.getDayOfWeek() != DayOfWeek.SUNDAY;
    }

}
