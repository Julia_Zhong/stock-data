package julia.stock.data;

import julia.stock.data.entity.EntryDate;
import julia.stock.data.entity.StockInfo;
import julia.stock.data.service.EntryDateService;
import julia.stock.data.service.StockInfoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
@RequiredArgsConstructor
public class StockDataFileParser {
    private final StockInfoService stockInfoService;
    private final EntryDateService dateService;

    public void readFile(Path fileToLoad, LocalDate date) throws IOException {
        log.info("Starting parsing file " + fileToLoad.toString());
        FileInputStream fis = new FileInputStream(fileToLoad.toFile());
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet sheet = workbook.getSheetAt(0);

        EntryDate entryDate = dateService.saveIfNotExists(date);

        for (Row row : sheet) {
            if (!(row.getRowNum() == 0)) {
                Map<Integer, String> cellValues = new HashMap<>();
                for (int i = row.getFirstCellNum(); i < row.getPhysicalNumberOfCells(); ++i) {
                    Cell cell = row.getCell(i, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                    cellValues.put(cell.getColumnIndex(), cell.toString());
                }
                StockInfo stockInfo = StockInfo.createEntity(cellValues, entryDate);
                stockInfoService.upseart(stockInfo, entryDate);
            }
        }
        workbook.close();
        fis.close();
        log.info("Reading completed " + fileToLoad.toString());
    }
}
