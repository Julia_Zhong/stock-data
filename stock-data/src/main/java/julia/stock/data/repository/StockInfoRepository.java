package julia.stock.data.repository;

import julia.stock.data.entity.EntryDate;
import julia.stock.data.entity.StockInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StockInfoRepository extends JpaRepository<StockInfo, Long> {

    StockInfo findByTickerAndIsinAndCurrencyAndDateEntry(String ticker, String isin, String currency, EntryDate dateEntry);

    List<StockInfo> findByDateEntry(EntryDate dateEntry);

    List<StockInfo> findTop3ByDateEntryAndPriceChangeNotNullOrderByPriceChangeDesc(EntryDate dateEntry);

    List<StockInfo> findTop3ByDateEntryAndPriceChangeNotNullOrderByPriceChangeAsc(EntryDate dateEntry);

    List<StockInfo> findTop6ByDateEntryAndTurnoverNotNullOrderByTurnoverDesc(EntryDate dateEntry);

    List<StockInfo> findTop6ByDateEntryAndTradesNotNullOrderByTradesDesc(EntryDate dateEntry);

}