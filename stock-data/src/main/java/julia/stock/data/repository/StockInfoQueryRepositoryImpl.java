package julia.stock.data.repository;

import julia.stock.data.dto.response.MostActiveDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
@Slf4j
@RequiredArgsConstructor
public class StockInfoQueryRepositoryImpl implements StockInfoQueryRepository{

    private final NamedParameterJdbcTemplate jdbcTemplate;

    private static final String QUERY = "SELECT name, SUM(volume) " +
            "AS volume FROM stock_data sd " +
            "WHERE date_id IN" +
            "(SELECT id FROM entry_date ed WHERE date BETWEEN :startDate and :endDate)" +
            " AND volume IS NOT NULL " +
            "GROUP BY name " +
            "ORDER BY volume DESC " +
            "LIMIT 10";

    @Override
    public List<MostActiveDto> findTopTenByVolumeForPeriod(LocalDate startDate, LocalDate endDate) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("startDate", startDate)
                .addValue("endDate", endDate);
        return jdbcTemplate.query(QUERY, params, new BeanPropertyRowMapper<>(MostActiveDto.class));
    }
}
