package julia.stock.data.repository;

import julia.stock.data.dto.response.MostActiveDto;

import java.time.LocalDate;
import java.util.List;

public interface StockInfoQueryRepository {

    List<MostActiveDto> findTopTenByVolumeForPeriod(LocalDate startDate, LocalDate endDate);
}
