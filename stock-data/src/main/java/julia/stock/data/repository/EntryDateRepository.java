package julia.stock.data.repository;

import julia.stock.data.entity.EntryDate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface EntryDateRepository extends JpaRepository<EntryDate, Long> {
    EntryDate findDateEntryByDate(LocalDate date);
    boolean existsByDate(LocalDate date);

    EntryDate findTopByOrderByIdDesc();
}
