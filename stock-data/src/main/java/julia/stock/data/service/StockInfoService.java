package julia.stock.data.service;

import julia.stock.data.dto.response.*;
import julia.stock.data.entity.EntryDate;
import julia.stock.data.entity.StockInfo;
import julia.stock.data.repository.EntryDateRepository;
import julia.stock.data.repository.StockInfoQueryRepository;
import julia.stock.data.repository.StockInfoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class StockInfoService {
    private final StockInfoRepository stockInfoRepository;
    private final StockInfoQueryRepository queryRepository;
    private final EntryDateRepository dateRepository;

    public StockDataResponse getStockData() {
        List<StockInfo> all = stockInfoRepository.findAll();
        return new StockDataResponse(stockDataDtoList(all));
    }


    public void upseart(StockInfo stockInfo, EntryDate entryDate) {
        StockInfo stockInfoToUpsert = stockInfoRepository.findByTickerAndIsinAndCurrencyAndDateEntry(
                stockInfo.getTicker(), stockInfo.getIsin(), stockInfo.getCurrency(), entryDate);
        if (stockInfoToUpsert == null) {
            stockInfoRepository.save(stockInfo);
        } else {
            stockInfo.setId(stockInfoToUpsert.getId());
            stockInfo.setCreated(stockInfoToUpsert.getCreated());
            stockInfoRepository.save(stockInfo);
        }
    }

    public MostActiveResponse getStockDataForPeriod(String from, String to) {
        List<MostActiveDto> topTen = queryRepository.findTopTenByVolumeForPeriod(LocalDate.parse(from), LocalDate.parse(to));
        return new MostActiveResponse(topTen);
    }

    public StockDataResponse getCurrentStockData() {
        EntryDate entryDate = dateRepository.findDateEntryByDate(LocalDate.now());
        List<StockInfo> stockInfoList = stockInfoRepository.findByDateEntry(entryDate);
        return new StockDataResponse(stockDataDtoList(stockInfoList));
    }
    private List<StockDataDto> stockDataDtoList(List<StockInfo> stockInfo) {
        return stockInfo.stream()
                .map(StockDataDto::convertToDto)
                .sorted(Collections.reverseOrder(Comparator.comparing(StockDataDto::getDate)))
                .collect(Collectors.toList());
    }

    public StockDataDailyResponse getDailyData(String date) {
        EntryDate entryDate = dateRepository.findDateEntryByDate(LocalDate.parse(date));
        List<StockInfo> topGained = stockInfoRepository.findTop3ByDateEntryAndPriceChangeNotNullOrderByPriceChangeDesc(entryDate);
        List<StockInfo> topDeclined = stockInfoRepository.findTop3ByDateEntryAndPriceChangeNotNullOrderByPriceChangeAsc(entryDate);
        List<StockInfo> mostActive = stockInfoRepository.findTop6ByDateEntryAndTradesNotNullOrderByTradesDesc(entryDate);
        List<StockInfo> biggestTurnover = stockInfoRepository.findTop6ByDateEntryAndTurnoverNotNullOrderByTurnoverDesc(entryDate);
        return StockDataDailyResponse.builder()
                .topGained(stockDataDtoList(topGained))
                .topDeclined(stockDataDtoList(topDeclined))
                .mostActive(stockDataDtoList(mostActive))
                .biggestTurnover(stockDataDtoList(biggestTurnover))
                .build();
    }
}

