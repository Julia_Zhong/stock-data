package julia.stock.data.service;

import julia.stock.data.dto.response.DateDto;
import julia.stock.data.dto.response.EntryDateResponse;
import julia.stock.data.entity.EntryDate;
import julia.stock.data.repository.EntryDateRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class EntryDateService {
    private final EntryDateRepository entryDateRepository;

    public EntryDate saveIfNotExists(LocalDate date) {
        if (entryDateRepository.existsByDate(date)) {
            return entryDateRepository.findDateEntryByDate(date);
        }
        return entryDateRepository.save(
                EntryDate.builder()
                        .date(date)
                        .build());
    }

    public EntryDateResponse getAvailableDates() {
        List<DateDto> dates = entryDateRepository.findAll().stream()
                .map(this::entryDateDto)
                .collect(Collectors.toList());
    return EntryDateResponse.builder()
            .availableDates(dates)
            .build();
    }

    private DateDto entryDateDto(EntryDate date) {
        return DateDto.builder().id(date.getId()).date(date.getDate()).build();
    }
}
