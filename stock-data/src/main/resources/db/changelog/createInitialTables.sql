CREATE TABLE entry_date
(
    id          BIGSERIAL PRIMARY KEY,
    date        DATE         NOT NULL,
    created     TIMESTAMP    NOT NULL,
    updated     TIMESTAMP    NOT NULL,
    UNIQUE(date)
);
CREATE TABLE stock_data
(
    id               BIGSERIAL PRIMARY KEY,
    ticker           VARCHAR(255) NOT NULL,
    name             VARCHAR(255) NOT NULL,
    isin             VARCHAR(255) NOT NULL,
    currency         VARCHAR(255) NOT NULL,
    market_place     VARCHAR(255),
    list_or_segment  VARCHAR(255),
    average_price    NUMERIC,
    open_price       NUMERIC,
    high_price       NUMERIC,
    low_price        NUMERIC,
    last_close_price NUMERIC,
    last_price       NUMERIC,
    price_change     NUMERIC,
    best_bid         NUMERIC,
    best_ask         NUMERIC,
    trades           NUMERIC,
    volume           NUMERIC,
    turnover         NUMERIC,
    industry         VARCHAR(255) NOT NULL,
    supersector      VARCHAR(255) NOT NULL,
    date_id          BIGINT REFERENCES entry_date (id) NOT NULL,
    created          TIMESTAMP    NOT NULL,
    updated          TIMESTAMP    NOT NULL,
    UNIQUE(ticker, isin, currency, date_id)
);

CREATE
OR REPLACE FUNCTION audit_tstamp() RETURNS trigger AS $audit_tstamp$
BEGIN
  IF
TG_OP = 'INSERT' THEN
      NEW.created := NOW();
END IF;

  NEW.updated
:= NOW();
RETURN NEW;
END;
$audit_tstamp$
LANGUAGE plpgsql;

CREATE TRIGGER stock_data_tstamp
    BEFORE INSERT OR UPDATE ON stock_data FOR EACH ROW
EXECUTE PROCEDURE audit_tstamp();
CREATE TRIGGER entry_date_tstamp
    BEFORE INSERT OR UPDATE ON entry_date FOR EACH ROW
EXECUTE PROCEDURE audit_tstamp();