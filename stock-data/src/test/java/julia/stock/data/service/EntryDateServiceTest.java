package julia.stock.data.service;

import julia.stock.data.entity.EntryDate;
import julia.stock.data.repository.EntryDateRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class EntryDateServiceTest {

    @InjectMocks
    EntryDateService dateService;
    @Mock
    EntryDateRepository dateRepository;

    private EntryDate date = EntryDate.builder()
            .id(1L)
            .date(LocalDate.of(2021, 12, 31)).build();

    @Test
    void test_saveIfNorExists_dateExists() {
        when(dateRepository.existsByDate(LocalDate.parse("2021-12-31"))).thenReturn(true);
        when(dateRepository.findDateEntryByDate(LocalDate.parse("2021-12-31"))).thenReturn(date);
        EntryDate testDate = dateService.saveIfNotExists(LocalDate.parse("2021-12-31"));

        verify(dateRepository).findDateEntryByDate(LocalDate.parse("2021-12-31"));
        assertEquals("2021-12-31", testDate.getDate().toString());


    }

    @Test
    void test_saveIfNorExists_dateDoesNotExists() {
        when(dateRepository.existsByDate(LocalDate.parse("2021-12-31"))).thenReturn(false);
        when(dateRepository.save(any())).thenReturn(date);
        EntryDate testDate = dateService.saveIfNotExists(LocalDate.parse("2021-12-31"));

        assertEquals("2021-12-31", testDate.getDate().toString());


    }
}
