package julia.stock.data.service;

import julia.stock.data.dto.response.MostActiveDto;
import julia.stock.data.dto.response.MostActiveResponse;
import julia.stock.data.dto.response.StockDataDailyResponse;
import julia.stock.data.dto.response.StockDataResponse;
import julia.stock.data.entity.EntryDate;
import julia.stock.data.entity.StockInfo;
import julia.stock.data.repository.EntryDateRepository;
import julia.stock.data.repository.StockInfoQueryRepository;
import julia.stock.data.repository.StockInfoRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class StockInfoServiceTest {
    @InjectMocks
    StockInfoService infoService;
    @Mock
    StockInfoRepository stockInfoRepository;
    @Mock
    EntryDateRepository entryDateRepository;
    @Mock
    StockInfoQueryRepository queryRepository;

    private EntryDate date = EntryDate.builder()
            .id(1L)
            .date(LocalDate.of(2021, 12, 31)).build();

    private StockInfo stockInfo = StockInfo.builder()
            .name("COMPANY")
            .currency("EUR")
            .industry("IT")
            .lastPrice(new BigDecimal("1.23"))
            .dateEntry(date)
            .build();

    private StockInfo topGained = StockInfo.builder()
            .name("COMPANY1")
            .currency("EUR")
            .industry("IT")
            .lastPrice(new BigDecimal("5.66"))
            .dateEntry(date)
            .build();
    private StockInfo topDeclined = StockInfo.builder()
            .name("COMPANY2")
            .currency("EUR")
            .industry("IT")
            .lastPrice(new BigDecimal("3.22"))
            .dateEntry(date)
            .build();
    private StockInfo mostActive = StockInfo.builder()
            .name("COMPANY3")
            .currency("EUR")
            .industry("IT")
            .lastPrice(new BigDecimal("8.89"))
            .dateEntry(date)
            .build();
    private StockInfo biggestTurnover = StockInfo.builder()
            .name("COMPANY4")
            .currency("EUR")
            .industry("IT")
            .lastPrice(new BigDecimal("0.12"))
            .dateEntry(date)
            .build();
    private MostActiveDto mostActivePeriod = MostActiveDto.builder()
            .name("COMPANY")
            .volume(new BigDecimal("1111")).build();


    @Test
    void test_getStockData() {
        when(stockInfoRepository.findAll()).thenReturn(List.of(stockInfo));
        StockDataResponse testResponse = infoService.getStockData();
        assertEquals(2021, LocalDate.now().getYear());
        assertEquals("COMPANY", testResponse.getStockData().get(0).getName());
        assertEquals("EUR", testResponse.getStockData().get(0).getCurrency());
        assertEquals("IT", testResponse.getStockData().get(0).getIndustry());
        assertEquals("1.23", testResponse.getStockData().get(0).getLastPrice().toString());
        assertEquals("2021-12-31", testResponse.getStockData().get(0).getDate().toString());

    }

    @Test
    void test_getStockDataForPeriod() {

        when(queryRepository.findTopTenByVolumeForPeriod(any(LocalDate.class),any(LocalDate.class))).thenReturn(List.of(mostActivePeriod));

        MostActiveResponse testResponse = infoService.getStockDataForPeriod("2021-12-20", "2021-12-31");
        assertEquals("COMPANY", testResponse.getMostActiveList().get(0).getName());
        assertEquals("1111", testResponse.getMostActiveList().get(0).getVolume().toString());


    }

    @Test
    void test_getDailyData() {
        when(entryDateRepository.findDateEntryByDate(LocalDate.parse("2021-12-31"))).thenReturn(date);
        when(stockInfoRepository.findTop3ByDateEntryAndPriceChangeNotNullOrderByPriceChangeDesc(date)).thenReturn(List.of(topGained));
        when(stockInfoRepository.findTop3ByDateEntryAndPriceChangeNotNullOrderByPriceChangeAsc(date)).thenReturn(List.of(topDeclined));
        when(stockInfoRepository.findTop6ByDateEntryAndTradesNotNullOrderByTradesDesc(date)).thenReturn(List.of(mostActive));
        when(stockInfoRepository.findTop6ByDateEntryAndTurnoverNotNullOrderByTurnoverDesc(date)).thenReturn(List.of(biggestTurnover));

        StockDataDailyResponse testResponse = infoService.getDailyData("2021-12-31");

        assertNotNull(testResponse);
        assertEquals("COMPANY1", testResponse.getTopGained().get(0).getName());
        assertEquals("EUR", testResponse.getTopGained().get(0).getCurrency());
        assertEquals("IT", testResponse.getTopGained().get(0).getIndustry());
        assertEquals("5.66", testResponse.getTopGained().get(0).getLastPrice().toString());
        assertEquals("2021-12-31", testResponse.getTopGained().get(0).getDate().toString());

        assertEquals("COMPANY2", testResponse.getTopDeclined().get(0).getName());
        assertEquals("EUR", testResponse.getTopDeclined().get(0).getCurrency());
        assertEquals("IT", testResponse.getTopDeclined().get(0).getIndustry());
        assertEquals("3.22", testResponse.getTopDeclined().get(0).getLastPrice().toString());

        assertEquals("COMPANY3", testResponse.getMostActive().get(0).getName());
        assertEquals("EUR", testResponse.getMostActive().get(0).getCurrency());
        assertEquals("IT", testResponse.getMostActive().get(0).getIndustry());
        assertEquals("8.89", testResponse.getMostActive().get(0).getLastPrice().toString());

        assertEquals("COMPANY4", testResponse.getBiggestTurnover().get(0).getName());
        assertEquals("EUR", testResponse.getBiggestTurnover().get(0).getCurrency());
        assertEquals("IT", testResponse.getBiggestTurnover().get(0).getIndustry());
        assertEquals("0.12", testResponse.getBiggestTurnover().get(0).getLastPrice().toString());
    }
}
