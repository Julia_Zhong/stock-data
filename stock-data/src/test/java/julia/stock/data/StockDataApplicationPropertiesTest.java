package julia.stock.data;

import julia.stock.data.config.StockDataApplicationProperties;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@TestPropertySource(properties = {
        "stock.data.endpoint: file/path/"
})

public class StockDataApplicationPropertiesTest {
    @Autowired
    StockDataApplicationProperties properties;

    @Test
    public void testUrlFetching() {
        String url = properties.getEndpoint();
        assertEquals("file/path/", url);

    }

    @Configuration
    @EnableConfigurationProperties(
            StockDataApplicationProperties.class
    )
    static class Cfg {
    }

}
