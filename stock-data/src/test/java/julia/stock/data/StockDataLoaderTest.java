package julia.stock.data;

import julia.stock.data.config.StockDataApplicationProperties;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyObject;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class StockDataLoaderTest {
    @InjectMocks
    StockDataLoader dataLoader;
    @Mock
    StockDataApplicationProperties properties;
    @Mock
    RestTemplate restTemplate;
    @Captor
    ArgumentCaptor<String> endpointCaptor;

    @Test
    void successfullyBuildRequestUri(){
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("https://nasdaqbaltic.com/statistics/en/shares")
                .queryParam("download", 1)
                .queryParam("date", "2021-10-09");
        assertEquals("https://nasdaqbaltic.com/statistics/en/shares?download=1&date=2021-10-09", builder.toUriString());

//        doNothing().when(restTemplate).exchange(endpointCaptor.capture(), any(),any(HttpEntity.class), anyObject());
//        restTemplate.exchange(builder.toUriString(), HttpMethod.GET, any(), byte[].class);
//
//        assertEquals("https://nasdaqbaltic.com/statistics/en/shares?download=1&date=2021-10-09", endpointCaptor.getValue());

    }

}
